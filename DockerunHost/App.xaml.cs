﻿using System;
using System.Windows;
using Microsoft.Win32;

namespace DockerunHost
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        internal const string UriScheme = "dockerun";
        internal const string FriendlyName = "Dockerun Protocol";

        public static void RegisterUriScheme()
        {
            using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\" + UriScheme))
            {
                // Replace typeof(App) by the class that contains the Main method or any class located in the project that produces the exe.
                // or replace typeof(App).Assembly.Location by anything that gives the full path to the exe
                string applicationLocation = Environment.GetCommandLineArgs()[0];

                key.SetValue("", "URL:" + FriendlyName);
                key.SetValue("URL Protocol", "");

                using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
                {
                    defaultIcon.SetValue("", System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\logo.png");
                }

                using (var commandKey = key.CreateSubKey(@"shell\open\command"))
                {
                    commandKey.SetValue("", "\"" + applicationLocation + "\" \"%1\"");
                }

                MessageBox.Show("Dockerun Host registered with OS", "Success!", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            if (e.Args.Length > 0)
            {
                if (Uri.TryCreate(e.Args[0], UriKind.Absolute, out var uri) &&
                    string.Equals(uri.Scheme, App.UriScheme, StringComparison.OrdinalIgnoreCase))
                {
                    ConfirmRun cr = new ConfirmRun();
                    cr.Show();
                    return;
                }
            }

            RegisterUriScheme();
        }
    }
}
